# XRootD Disk Cache Local Access

## Description
A Vagrant XRootD Setup utilising a serverside redirection plug-in to redirect clients to the mounted shared filesystem 
##Usage
-git clone https://git.gsi.de/dc/XRootD-Reps/xrootd-disk-cache-local-access.git --recursive
-cd alice-xrootd-builder-docker-container/
-sudo ./start.sh
-cd ..
-vagrant up
-vagrant ssh client
